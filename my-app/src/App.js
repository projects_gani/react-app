import React from 'react';
import './App.css';

function App() {
  function changeTheme() {
    const body = document.querySelector("body");
    const planets = document.querySelector(".planets-list");
    body.classList.toggle("dark");
    planets.classList.toggle("active");
  }

  let element = (
    <label className="switch" htmlFor="checkbox">
    <input type="checkbox" id="checkbox"/>
    <div className="slider round" onClick={changeTheme}></div>
    </label>
  );

  return React.createElement(
    'div',
    null,
    React.createElement('h1', { style: { color: '#999', fontSize: '19px' } }, 'Solar system planets'),
    <ul className='planets-list'>
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>,
    element
  );
}

export default App;
